#!/usr/bin/env python
import itk
import sys
itk.auto_progress(2)

if len(sys.argv) < 2:
    print("Usage: " + sys.argv[0] +
          " <InputNIFTIFileName> <OutputDirectory> ")
    sys.exit(1)

inputNIFTI = sys.argv[1]
outputDir = sys.argv[2]

NIFTIPixelType = itk.ctype('float')
NIFTIImageType = itk.Image[NIFTIPixelType, 3]

# Read the input B1 image with low FA (Nifti image)
fileReader = itk.ImageFileReader[NIFTIImageType].New()
fileReader.SetFileName(inputNIFTI)
fileReader.Update()

medianFilterType = itk.MedianImageFilter[NIFTIImageType, NIFTIImageType]
medianFilter = medianFilterType.New()
medianFilter.SetInput(fileReader.GetOutput())

otsuFilterType = itk.OtsuThresholdImageFilter[NIFTIImageType, itk.Image[itk.UC, 3]]
otsuFilter = otsuFilterType.New()
otsuFilter.SetOutsideValue(1)
otsuFilter.SetInsideValue(0)
otsuFilter.SetNumberOfHistogramBins(200)
otsuFilter.SetInput(medianFilter.GetOutput())

# self.writeNii('otsu.nii.gz', otsuFilter.GetOutput())

# biasFilterType = itk.N4BiasFieldCorrectionImageFilter[NIFTIImageType, itk.Image[itk.UC, 3], NIFTIImageType]
biasFilterType = itk.N4BiasFieldCorrectionImageFilter[NIFTIImageType, itk.Image[itk.UC, 3], NIFTIImageType]
biasFilter = biasFilterType.New()
fittingLevels = [3, 3, 3]
biasFilter.SetNumberOfFittingLevels(fittingLevels)
biasFilter.SetMaximumNumberOfIterations([50, 40, 30])
biasFilter.SetConvergenceThreshold(0.0001)
numberOfControlPoints = []
for level in fittingLevels:
    numberOfControlPoints.append(level + biasFilter.GetSplineOrder())
biasFilter.SetNumberOfControlPoints(numberOfControlPoints)
biasFilter.SetNumberOfThreads(8)

biasFilter.SetMaskImage(otsuFilter.GetOutput())
biasFilter.SetInput(fileReader.GetOutput())
biasFilter.Update()

WriterType = itk.ImageFileWriter[NIFTIImageType]
writer = WriterType.New()
writer.SetInput(biasFilter.GetOutput())
writer.SetFileName(outputDir + '/' + 'bias_corrected_image' + '.nii')
writer.Update()
